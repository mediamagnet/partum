window.setAttributeLevel = function(event) {
    const target = event.target || event.srcElement;
    const group = target.parentElement.id;
    const levels = document.getElementById(group).getElementsByTagName("input");
    for (let level of levels) {
        level.checked = parseInt(level.value) <= parseInt(target.value);
    }
};
document.addEventListener("DOMContentLoaded", function(){
    console.log("potato");
    document.getElementById("SplatBook").addEventListener("change", () => {
        let splat = document.getElementById("SplatBook").value;
        console.log(splat);
        switch (splat) {
            // with thanks to KageYuuki for digging Virtues and Vices up.
            case "Changeling: The Lost":
                document.getElementById("vir").innerHTML = "Needle:";
                document.getElementById("vic").innerHTML = "Thread:";
                document.getElementById("a-span").innerHTML = "Seeming:";
                document.getElementById("b-span").innerHTML = "Kith:";
                document.getElementById("c-span").innerHTML = "Court:";
                break;
            case "Vampire: The Requiem":
                document.getElementById("vir").innerHTML = "Mask:";
                document.getElementById("vic").innerHTML = "Dirge:";
                document.getElementById("a-span").innerHTML = "Clan:";
                document.getElementById("b-span").innerHTML = "Bloodline:";
                document.getElementById("c-span").innerHTML = "Covenant:";
                break;
            case "Werewolf: The Forsaken":
                document.getElementById("vir").innerHTML = "Blood:";
                document.getElementById("vic").innerHTML = "Bone:";
                document.getElementById("a-span").innerHTML = "Auspice:";
                document.getElementById("b-span").innerHTML = "Tribe:";
                document.getElementById("c-span").innerHTML = "Lodge:";
                break;
            case "Demon: The Decent":
                document.getElementById("vir").innerHTML = "Virtue:";
                document.getElementById("vic").innerHTML = "Vice:";
                document.getElementById("a-span").innerHTML = "Incarnation:";
                document.getElementById("b-span").innerHTML = "Agenda:";
                document.getElementById("c-span").innerHTML = "Catalyst:";
                break;
            case "Mage: The Awakening":
                document.getElementById("vir").innerHTML = "Virtue:";
                document.getElementById("vic").innerHTML = "Vice:";
                document.getElementById("a-span").innerHTML = "Path:";
                document.getElementById("b-span").innerHTML = "Order:";
                document.getElementById("c-span").innerHTML = "Legacy:";
                break;
            case "Geist: The Sin-Eaters":
                document.getElementById("vir").innerHTML = "Root:";
                document.getElementById("vic").innerHTML = "Bloom:";
                document.getElementById("a-span").innerHTML = "Archetype:";
                document.getElementById("b-span").innerHTML = "Burden:";
                document.getElementById("c-span").innerHTML = "Krewe:";
                break;
            case "Beast: The Primordial":
                document.getElementById("vir").innerHTML = "Legend:";
                document.getElementById("vic").innerHTML = "Life:";
                document.getElementById("a-span").innerHTML = "Family:";
                document.getElementById("b-span").innerHTML = "Hunger:";
                document.getElementById("c-span").innerHTML = "Soul:";
                break;
            case "Mummy: The Curse":
                document.getElementById("vir").innerHTML = "Virtue:";
                document.getElementById("vic").innerHTML = "Vice:";
                document.getElementById("a-span").innerHTML = "Decree:";
                document.getElementById("b-span").innerHTML = "Guild:";
                document.getElementById("c-span").innerHTML = "Judge:";
                break;
            case "Promethean: The Created":
                document.getElementById("vir").innerHTML = "Elpis:";
                document.getElementById("vic").innerHTML = "Torment:";
                document.getElementById("a-span").innerHTML = "Linage:";
                document.getElementById("b-span").innerHTML = "Refinement:";
                document.getElementById("c-span").innerHTML = "Role:";
                break;
            case "Hunter: The Vigil":
                document.getElementById("vir").innerHTML = "Virtue:";
                document.getElementById("vic").innerHTML = "Vice:";
                document.getElementById("a-span").innerHTML = "Profession:";
                document.getElementById("b-span").innerHTML = "Compact:";
                document.getElementById("c-span").innerHTML = "Conspiracy:";
                break;
        }
    });
});
